const express = require("express")
app = express() 
var bodyParser=require("body-parser");

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(function (req, res, next) {

    res.header("Access-Control-Allow-Origin", "*");

    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    next();

});

app.post("/carzona",function(req,res){

    if(req.body.s_task=='fetchVehicles'){
        const startScript=require("./Fetch_List");
        startScript(req,function(vehicleDataset){
            res.send(vehicleDataset);
        });
    } else{
        console.log('Failure')
    }
})

function init(){
    app.listen(8015, function(){
        console.log('Server started at port 8015')
    })
    console.log('Server initiating')
}
init()

