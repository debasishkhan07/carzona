var app = angular.module('xapp', []);

function printName(){
    document.getElementById("mySpan2").innerHTML="Mars";
}
app.controller('testControl',['$scope','$http', function($scope, $http){
    $scope.printX = function(){
        $scope.outCome = "Loading data";
        fetcher($scope, $http)
    }
}])

function fetcher($scope, $http){
    try{
        $http({
            url: "http://localhost:8015/carzona/",
            method: "post",
            headers: {'Content-Type': 'application/json'},
            data: {'s_task':'fetchVehicles'},
            dataType : "text"
        }).then(function(data){
            $scope.outCome = data;
        },function (err) {
            alert("Post Error : "+JSON.stringify(err));
        });
    }catch(err){
        alert(err)
    }
}